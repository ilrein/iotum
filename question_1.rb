def question_1_as_array
  returnArrayValue = []
  (1..100).each do |n|
    returnArrayValue.push(n)
  end
  return returnArrayValue
end

def question_1_with_foo
  returnArrayValue = []
  (1..100).each do |n|
    if n % 3 == 0
      returnArrayValue.push('Foo')
    else
      returnArrayValue.push(n)
    end
  end
  return returnArrayValue
end

def question_1_with_bar
  arr = []
  (1..100).each do |n|
    if n % 3 === 0
      arr.push('Foo')
    end
    if n % 5 === 0
      arr.push('Bar')
    end
  end
  return arr
end

def question_1_with_foobar
  arr = []
  (1..100).each do |n|
    if ((n % 5 == 0) && (n % 3 == 0))
      arr.push('FooBar')
    elsif (n % 3 === 0)
      arr.push('Foo')
    elsif (n % 5 === 0)
      arr.push('Bar')
    else
      arr.push(n)
    end
  end
  return arr
end

def question_1
  arr = []
  (1..100).each do |n|
    if ((n % 5 == 0) && (n % 3 == 0))
      arr.push('FooBar')
    elsif (n % 3 === 0)
      arr.push('Foo')
    elsif (n % 5 === 0)
      arr.push('Bar')
    else
      arr.push(n)
    end
  end
  outputString = ''
  arr.each_with_index do |item, index|
    # nicer formatting
    if index == 0
      outputString << "#{item}"
    else
      outputString << " #{item}"
    end
  end
  return outputString
end
