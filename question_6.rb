def question_6(array, intervalSize)
  returnValue = [0, 0]
  chunked_array = array.each_slice(intervalSize).to_a

  chunked_array.each_with_index do |value, index|
    total = (value[0] + value[1])
    if (returnValue[0][0] < total)
      returnValue.clear
      returnValue.push(total, (index * 2))
    end
  end
  return returnValue
end
