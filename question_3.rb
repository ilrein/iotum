def question_3(min, max, inputs = 3)
  res = rand(min..max)
  res *= inputs
  return res
end
