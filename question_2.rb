def even_integers(num)
  total = 0
  res = Array (0..num)
  res.each do |n|
    if n % 2 == 0
      total += 1
    end
  end
  return total
end
