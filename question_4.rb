# Remove the duplicates.
def question_4
  words = ['one', 'one', 'two', 'three', 'three', 'two']
  return words.uniq!
end
