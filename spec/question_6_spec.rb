require_relative '../question_6'

RSpec.describe 'Question 6' do
  context "Takes an array and splits it into subarrays" do
    it "Returns largest sum and index of first entry" do
      data = [1, 1, 1, 1, 1, 1, 1, 2]
      expect(question_6(data, 2)).to eq [3, 6] 
    end
  end
end
