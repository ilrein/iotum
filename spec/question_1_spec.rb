require_relative '../question_1'

RSpec.describe 'Question 1', '#Fizzbuzz ;)' do
  context "it outputs various strings, in a loop of 100, based on a divisible" do
    it "should initially output a loop from 1..100" do
      expect(question_1_as_array).to eq Array (1..100)
    end
    it "outputs 'Foo' if the index is divisible by 3" do
      expect(question_1_with_foo[2]).to eq 'Foo'
    end
    it "outputs 'Bar' if the index is divisible by 5" do
      expect(question_1_with_bar[4]).to eq 'Bar'
    end
    it "outputs 'FooBar' if the index is divisible by 5" do
      expect(question_1_with_foobar[14]).to eq 'FooBar'
    end
    it "the final output should be a string" do
      expect(question_1_with_foobar.join(' ')).to eq question_1
    end
  end
end
