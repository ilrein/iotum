require_relative '../question_3'

RSpec.describe 'Question 3' do
  context "Range of possible values for A" do
    it "counts possible ranges automatically" do
      expect(3..18).to cover(question_3(1, 6))
    end
  end
end
