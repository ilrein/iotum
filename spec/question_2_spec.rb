require_relative '../question_2'

RSpec.describe 'Question 2' do
  context "counts the number of even numbers that appear in a range from 0" do
    it "counts two even numbers from an input of 3" do
      expect(even_integers(3)).to eq 2
    end
    it "counts four even numbers from an input of 6" do
      expect(even_integers(6)).to eq 4
    end
  end
end
