require_relative '../question_4'

RSpec.describe 'Question 4' do
  context "Removing duplicates from an array" do
    it "counts possible ranges automatically" do
      expect(question_4).to eq ['one', 'two', 'three']
    end
  end
end
